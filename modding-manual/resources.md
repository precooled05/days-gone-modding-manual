# Resources
[< < <](../readme.md)

- Misc resources: https://drive.google.com/drive/folders/15EfpyySqGF7s4DsyZspVr7lI0uGg9sa9

## Tools
TODO?: add a small description nested inside each tool

- Uasset Editor: https://github.com/kaiheilos/Utilities
- Unofficial SDK: https://github.com/patrickBakin/DaysGoneSDK
- UEViewer (set Unreal Engine 4.11, Days Gone, PC): https://www.gildor.org/en/projects/umodel
- 3ds Max and ActorX plug-in: https://www.gildor.org/projects/unactorx
- Blender psk / psa add-on: https://github.com/Befzz/blender3d_import_psk_psa

## Specifications

- Unreal Engine Version: `4.11.2`
- Rendering API: `DX11`
- Save file location: `%localappdata%\BendGame\Saved`
- Main config file path: `%localappdata%\BendGame\Saved\Config\WindowsNoEditor`
- Secondary config file path: `%localappdata%\DaysGone\4.11\Saved\Config\WindowsNoEditor`
- SteamOS mod directory: `.local\share\Steam\steamapps\compatdata\1259420\pfx\pfx\drive_c\users\steamuser\AppData\Local\BendGame`

[< < <](../readme.md)
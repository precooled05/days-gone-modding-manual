# Using UassetEditor
[< < <](../../readme.md)

TODO: Add "UassetEditor is"

Download: https://github.com/kaiheilos/Utilities <br>
How to use the Uasset Editor by KaiHeilos: https://www.youtube.com/watch?v=4_Ld7fiLFZw

> **Note**: in the top right of the Uasset Editor window set the UE mode to 4.11

Linked Classes, Imports and References with the Uasset Editor: https://www.youtube.com/watch?v=WmhVFM4OIGw


## Dumping Game Assets

Requirements: at least 50GB free on your storage device

It is recommended to utilize the Uasset Editor to dump game files- as using tools like UModel will only dump graphical assets, we want to ensure all files are dumped. Fmodel is currently unsupported.

1. Navigate to Days `Gone\BendGame\Content\Paks`.

2. Open the Uasset Editor, from the functions drop-down menu select `Function -> Extract Pak File`.

3. Shift-click both `BendGame-WindowsNoEditor.paks`, choose destination folder for dumped assets.

4. Wait until entire process is completed, takes approximately 15-30 minutes, total contents of both .pak files are 42GB.

Afterwards, Umodel can be used to extract content from .uasset files: https://www.gildor.org/en/projects/umodel.

## Create a new .pak file

1. Recreate your modified file(s) original folder structure(s), starting at BendGame; e.g. `\BendGame\Content\Characters\Deacon`.

2. Open the Uasset Editor, from the functions drop-down menu select Function -> Package Folder.

3. Select the BendGame folder you created which contains your replicated folder structure.

<br>

[< < <](readme.md)
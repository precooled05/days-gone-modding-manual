# Getting Started
[< < <](../readme.md)

# Setting Up an Envoronment

## Install Debug Menu Mod
Install this mod: https://www.nexusmods.com/daysgone/mods/56 <br>
How to use it: https://docs.google.com/document/d/15oMMhBkJxYRBWo02RwbDqkzbjy1zqWuTU5ecODzsLQY

## Compile Unreal Engine
It is highly recommended that you compile Unreal Engine yourself in order to avoid a major bug with the release binary provided through the Ebig Lames Launcher, and to bake in support for UnrealModLoader.

Make sure you have at least 50gb of free hard drive space before you start.

1. Link your Epic and Github accounts, then join the Epic Games organization on Github
https://www.unrealengine.com/en-US/ue-on-github
2. Download the 4.11.2 source code (you must link your Epic and Github accounts in order to view it.)
https://github.com/EpicGames/UnrealEngine/tree/4.11.2-release
3. Download Visual Studio 2015 Update 2 (specify custom install, check C++ support while installing, do not check Update 3.)
https://gist.github.com/wang-bin/09e8182ae78e6d727721a02fe541150e
4. Run the Setup.bat inside the 4.11.2 source folder (downloads 3-4gb of binaries).
5. Run the GenerateProjectFiles.bat inside the 4.11.2 source folder (may take upwards to five minutes to create.)
6. Open Visual Studio and load the 4.11.2 source project from the UE4.sln file .
    - To fix the broken buttons/context menus bug, navigate inside the solution explorer to \Engine\Source\Runtime\Core\Private\Windows and find WindowsWindow.cpp, comment out line 82.
    - To add UnrealModLoader support by enabling chunk generation, navigate inside the solution explorer to Engine\Source\Editor\UnrealEd\Private\Commandlets and find ChunkManifestGenerator.cpp, comment out line 1285
    (use // at the start of a line to make a single line comment.)
7. Set the VS environment configuration to development editor and the platform to Win64.
8. Right click the UE4 target inside the solution explorer and select build
    - Wait until the build is finished and the new executable will be created, build will take approximately 25 minutes


[< < <](../readme.md)
# Using UnrealModLoader
[< < <](readme.md)

TODO: Add images!

UnrealModLoader allows you to load Blueprint and basic SDK based C++ mods in a standardized way, a powerful utility.

Download: https://github.com/patrickBakin/DaysGoneUnrealModLoader <br>

TesterMod.pak TODO: Figure out what to do about hosted files.

## Tutorials

Usage playlist by RussellJ: https://www.youtube.com/playlist?list=PL-dFOLrGFgdwbzcHmZ2ghuN3LXxlazbZP

Blueprint modding in Unreal Engine: https://www.youtube.com/watch?v=Qhrq7WGC3R4

Blueprint modding with UnrealModLoader: https://www.youtube.com/watch?v=eV2ILilbVlA

[< < <](readme.md)
# Using Mods
[< < <](../readme.md)

## Enabling Mods
1. Navigate to `Days Gone\BendGame\Contents`.
2. Rename startuppackages.pak to disabled_startuppackages.pak, or something similar.

There is an old method of enabling mods, but it is considered outdated, and should only be used if you encounter a mod that does not work with the above method.

1. Navigate to `Days Gone\BendGame\Contents`.
2. Rename the `sfpaks` directory to `sfpaks_disabled`, or something similar.

## Installing Mods

Most mods will give you their own instructions on how to install, but for those that don't:
- if all you have is a .pak file, place it in `%LocalAppData%\BendGame\Saved\Paks`

## Mod Manager

TODO:

[< < <](../readme.md)
# Days Gone Modding Manual
**Work in Progress, please forgive (and report to us) any missing information or mistakes.**

This is the new home of the guides formally found in the [Days Gone Modding](https://discord.gg/KTH2KTKUSZ) discord server.

If you're looking for a community centered around modding Days Gone, check it out: https://discord.gg/KTH2KTKUSZ

## Notice
All content is intended purely for the purposes of academic research.
Project maintainers are not responsible or liable for misuse of this documentation or any software referenced.

We provide no guarantee of support.
Use responsibly.

## Contents

`[X] = missing; [-] = in progress`

### User Manual
- [Using Mods](user-manual/using-mods.md) [-]
- [Reshade](user-manual/reshade.md) [X]
- [Block Data Collection](user-manual/data-collection.md) [X]
- [Linux](user-manual/linux.md) [X]

### Modding Manual
- [Getting Started](modding-manual/getting-started.md) [-]
- [Resources](modding-manual/resources.md) [-]
- /guides/
    - [Using UassetEditor](modding-manual/guides/using-uasseteditor.md) [-]
    - [Using Umodel](modding-manual/guides/using-umodel.md) [X]
    - [Using UnrealModLoader](modding-manual/guides/using-unrealmodloader.md) [-]
    - [Custom Textures](modding-manual/guides/custom-textures.md) [X]
    - [Importing New Assets](modding-manual/guides/import-assets.md) [X]

## Issues / Feedback

TODO:

## Offline Viewing

Simply clone/download this repository, open the directory in something like VSCode, and enable markdown preview.

## Contributing

TODO: 

## Credits
Edward [PRE]:
- Creator & Primary Maintainer (for both this repo, and its accompanying discord server)
- Website: [preProductions.net](https://www.preProductions.net)
- Twitter: [precooled05](https://twitter.com/precooled05)

## License

TODO: